from flask import Flask, request
app = Flask(__name__)


@app.route('/')
def index():
    return 'Hello Bob from 정금종(6310)'

@app.route('/<uri>', methods=['GET'])
def get(uri):
    param = request.args.to_dict()
    if uri == "add":
        return str(int(param['a']) + int(param['b']))
    elif uri == "sub":
        return str(int(param['a']) - int(param['b']))
    
    return '404 Not Found', 404

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8155)