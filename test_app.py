import unittest
import app



class APITest(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_add(self):
        res = self.app.get('/add?a=1&b=2')
        self.assertEqual(res.get_data(), b'3')

        res = self.app.get('/add?a=10000&b=2')
        self.assertEqual(res.get_data(), b'10002')

        res = self.app.get('/add?a=3&b=5')
        self.assertEqual(res.get_data(), b'8')


    def test_sub(self):
        res = self.app.get('/sub?a=1&b=2')
        self.assertEqual(res.get_data(), b'-1')

        res = self.app.get('/sub?a=10000&b=2')
        self.assertEqual(res.get_data(), b'9998')

        res = self.app.get('/sub?a=3&b=5')
        self.assertEqual(res.get_data(), b'-2')


if __name__ == "__main__":
    unittest.main()